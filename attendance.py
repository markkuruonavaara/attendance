import os
import requests
import json
import sys
import getopt
import datetime
#import MFRC522.MFRC522 as MFRC522
from pirc522 import RFID
import time

# default values for command line arguments
protocol = "http"
host = "localhost:3000"
debug = False

# reader instance
rdr = RFID()

# utility function for debug outputs
def debugPrint(*args):
    if debug:
        for a in args:
            print(a)

# utility function to simplify the code a bit
def makeURL(path):
    return protocol + "://" + host + path

# parse command line arguments
try:
    opts, args = getopt.getopt(sys.argv[1:], "du:p:")
except getopt.GetoptError:
    print("Usage: attendance.py [-d] [-p <protocol>] [-u <host>]")
    sys.exit(2)

for o, a in opts:
    if o == "-p":
        protocol = a
    if o == "-u":
        host = a
    if o == "-d":
        debug = True

# start read loop
print("Using " + makeURL(""))

while True:
    # sys.stderr.write("\x1b[2J\x1b[H")

    print("Ready to read tag")
    
    # read tag
    rdr.wait_for_tag()

    (error, data) = rdr.request()
    
    if not error:
        print("\nDetected: " + format(data, "02x"))

    tagId = ""

    (error, uid) = rdr.anticoll()
    if not error:
        tagId = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
        print("Tag read, id " + tagId)
        # print("Setting tag")
        # util.set_tag(uid)
        # print("\nAuthorizing")
        #util.auth(rdr.auth_a, [0x12, 0x34, 0x56, 0x78, 0x96, 0x92])
        # util.auth(rdr.auth_b, [0x74, 0x00, 0x52, 0x35, 0x00, 0xFF])
        # print("\nReading")
        # util.read_out(4)
        # print("\nDeauthorizing")
        # util.deauth()

    
    # read tag
    # tagId = input("Tag id: ");

    if tagId == "":
        continue

    # tag found, request status
    paramStr = {'tagId': tagId}
    resp = requests.get(makeURL("/attendance"), params = paramStr)
    debugPrint(resp.url)
    respStatus = resp.status_code
    respData = resp.json();

    debugPrint(resp.status_code)
    debugPrint(resp.text)

    if respStatus == 200:       # tag found
        print("Attendance status is:", respData['status'])

        checkedIn = True if respData['status'] == "IN" else False
    else:                       # tag unknown
        if input("Unknown tag, do you want to register it (y/n)? ").upper() == "Y":
            debugPrint("Registering")
            empId = input("Enter employee ID: ")
            # if ID validates OK:
            paramStr = { 'tagId': tagId, 'employeeId': empId }
            debugPrint(makeURL("/tag"), paramStr)
            resp = requests.post(makeURL("/tag"), data = paramStr)

            respStatus = resp.status_code
            respData = resp.json();

            debugPrint(resp.status_code)
            debugPrint(resp.text)
            debugPrint(respData)

            if respStatus == 201:
                print("Tag registered successfully to", respData['employeeId'])
            else:
                print("Registration failed")
            continue
        else:
            debugPrint("Cancelled")
            continue

    if checkedIn:
        msg = "Do you want to check out (y/n)? "
    else:
        msg = "Do you want to check in (y/n)? "

    if input(msg).upper() == "Y":
        checkedIn = not checkedIn;
        debugPrint("Post request")
        resp = requests.post(makeURL("/attendance"), params = paramStr)
        respStatus = resp.status_code
        respData = resp.json();
    else:
        continue

    debugPrint(resp.status_code)
    debugPrint(resp.text)

    if respStatus == 200:
        print("Attendance status is:", respData['status'])
        intime = int(respData['intime'])
        intimeStr = datetime.datetime.fromtimestamp(
            intime
            ).strftime('%Y-%m-%d %H:%M:%S')
        print("Check in time: ", intimeStr)
        if not checkedIn:
            outtime = int(respData['outtime'])
            outtimeStr = datetime.datetime.fromtimestamp(
                outtime
                ).strftime('%Y-%m-%d %H:%M:%S')

            print("Check out time: ", intimeStr)
            print("Working time today: ", str(datetime.timedelta(seconds=outtime-intime)))
    else:
        print("Error:", respData['error'])
        continue
