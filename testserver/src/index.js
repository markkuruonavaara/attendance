const express = require('express');
const morgan = require('morgan');
const winston = require('winston');
const bodyParser = require('body-parser');

const routes = require('./routes');

const app = express();

// Middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Routes
app.use('/', routes);

const port = 3000;
app.listen(port, () => {
  winston.info(`Listening on port ${port}`);
});
