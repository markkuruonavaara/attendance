const { Router } = require('express');
const winston = require('winston');

const router = new Router();
const database = require('./database');


router.post('/', (req, res) => {
  const { tagId, employeeId } = req.body;
  winston.info(`Add tag ${tagId} for employee ${employeeId}`);

  database[tagId] = {present: false, name: 'Etunimi Sukunimi', intime: null, outtime: null};

  res.status(201).send({
    tagId: tagId,
    employeeId: employeeId,
    name: database[tagId].name,
    intime: database[tagId].intime,
    outtime: database[tagId].outtime,
  });
});

module.exports = router;
