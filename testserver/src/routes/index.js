const { Router } = require('express');

const attendance = require('./attendance');
const tag = require('./tag');

const router = new Router();

router.use('/attendance', attendance);
router.use('/tag', tag);

module.exports = router;
