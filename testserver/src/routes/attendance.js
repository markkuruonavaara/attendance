const { Router } = require('express');
const winston = require('winston');

const router = new Router();
const database = require('./database.js');

router.get('/', (req, res) => {
  const { tagId } = req.query;
  winston.info(`Get status for tag ${tagId}`);

  if (tagId in database) {
    var attendance = database[tagId].present;

    winston.info(`Attendance status: ${attendance}`);
    winston.info({
      status: (attendance ? 'IN' : 'OUT'),
      name: database[tagId].name,
      intime: database[tagId].intime,
      outtime: database[tagId].outtime
    });
    res.send({
      status: (attendance ? 'IN' : 'OUT'),
      name: database[tagId].name,
      intime: database[tagId].intime,
      outtime: database[tagId].outtime
    });
  } else {
    res.status(404).send({
      error: 'Unknown tag',
    });
  }
});

router.post('/', (req, res) => {
  // const { tagId } = req.params;
  const { tagId, status } = req.body;
  winston.info(`Set status to ${status} for ${tagId}`);

  if (tagId in database) {
    database[tagId].present = !database[tagId].present;
    var attendance = database[tagId].present;
    if (attendance) {
      database[tagId].intime = Date.now()/1000;
      database[tagId].outtime = null;
    } else {
      database[tagId].outtime = Date.now()/1000;
    }

    winston.info(`Attendance status: ${attendance}`);
    res.send({
      status: (attendance ? 'IN' : 'OUT'),
      name: database[tagId].name,
      intime: database[tagId].intime,
      outtime: database[tagId].outtime
    });
  } else {
    res.status(404).send({
      error: 'Unknown tag',
    });
  }

});

module.exports = router;
